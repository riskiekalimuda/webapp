﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class NominalReader
    {
        #region Methods
        /// <summary>
        /// ConvertNominalToString
        /// </summary>
        /// <param name="nominal"></param>
        /// <returns></returns>
        public static string ConvertNominalToString(Int64 nominal)
        {
            string[] bilangan = { "", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas" };
            string result = "";

            if (nominal < 12)
            {
                result = " " + bilangan[nominal];
            }
            else if (nominal < 20)
            {
                result = ConvertNominalToString(nominal - 10).ToString() + " Belas";
            }
            else if (nominal < 100)
            {
                result = ConvertNominalToString(nominal / 10) + " Puluh" + ConvertNominalToString(nominal % 10);
            }
            else if (nominal < 200)
            {
                result = " Seratus" + ConvertNominalToString(nominal - 100);
            }
            else if (nominal < 1000)
            {
                result = ConvertNominalToString(nominal / 100) + " Ratus" + ConvertNominalToString(nominal % 100);
            }
            else if (nominal < 2000)
            {
                result = " Seribu" + ConvertNominalToString(nominal - 1000);
            }
            else if (nominal < 1000000)
            {
                result = ConvertNominalToString(nominal / 1000) + " Ribu" + ConvertNominalToString(nominal % 1000);
            }
            else if (nominal < 1000000000)
            {
                result = ConvertNominalToString(nominal / 1000000) + " Juta" + ConvertNominalToString(nominal % 1000000);
            }
            else if (nominal < 1000000000000)
            {
                result = ConvertNominalToString(nominal / 1000000000) + " Miliyar" + ConvertNominalToString(nominal % 1000000000);
            }
            return result;
        }
        #endregion
    }
}
