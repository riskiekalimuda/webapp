﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum EnumNotificationMessageType
    {
        NONE=0,
        ALERT_SUCCESS = 1,
        ALERT_FAILED =2
    }
}
