﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum EnumDebitNoteStatus
    {
        NONE = 0,
        PENDING=1,
        APPROVED=2,
        REJECTED = 3,
        RETURN_TO_CUSTOMER = 4,
        COMPLETED = 5,
        HOLD = 6
    }
}
