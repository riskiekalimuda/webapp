﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Constants
    {
        public static readonly string ACTION_FAILED_TITLE = "Failed.";
        public static readonly string ACTION_SUCCEEDED_TITLE = "Succeeded.";
        public static readonly string DELETE_FAILED_DESCRIPTION = "{0} delete failed.";
        public static readonly string DELETE_SUCCEEDED_DESCRIPTION = "{0} deleted.";
        public static readonly string SAVE_FAILED_DESCRIPTION = "Data {0} save failed.";
        public static readonly string DATA_IS_EXIST_DESCRIPTION = "Data {0} is exist.";
        public static readonly string DATA_IS_EMPTY = "{0} is empty.";
        public static readonly string SAVE_SUCCEEDED_DESCRIPTION = "Data {0} saved.";
        public static readonly string UPLOAD_DESCRIPTION = "{0} baris berhasil diunggah.";
        public static readonly string INVALID_PASSWORD = "Invalid Password";
    }
}
