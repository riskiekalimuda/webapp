﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DataTablesSearch
    {
        public Int32 Column { get; set; }
        public Boolean Searchable { get; set; }
        public String Search { get; set; }
        public Boolean Regex { get; set; }
    }
}
