﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class NotificationMessage
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }

        public static NotificationMessage Compose(String title, String description, EnumNotificationMessageType type)
        {

            NotificationMessage notif = new NotificationMessage()
            {
                Title = title,
                Description = description,
                Type = (type == EnumNotificationMessageType.ALERT_SUCCESS) ? "alert-success" :
                       (type == EnumNotificationMessageType.ALERT_FAILED) ? "alert-danger" : ""

            };
            return notif;
        }
    }
}
