﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMI.Common;

namespace Common
{
    public class DataTablesPostParam
    {
        public const Int16 MAX_COLS = 50;   // maksimal kolom yang dapat ditampung

        public Int32 DisplayStart { get; set; }  // display start point in the current data set
        public Int32 DisplayLength { get; set; } // number of records that the table can display in the current draw table
        public Int32 Columns { get; set; }       // number of columns being displayed
        public String Search { get; set; }       // global search field
        public Boolean IsRegex { get; set; }     // indicate whether the search keyword is regex
        public Int32 SortingColumns { get; set; } // number of columns to sort on
        public List<DataTablesSearch> Searches = new List<DataTablesSearch>();
        public List<DataTablesSort> Sorts = new List<DataTablesSort>();
        public String Echo { get; set; }


        public static DataTablesPostParam ExtractRequestParams(NameValueCollection paramRequest)
        {
            DataTablesPostParam extractedParam = null;

            if (paramRequest != null)
            {
                extractedParam = new DataTablesPostParam();
                DataTablesSort[] sortParams = new DataTablesSort[MAX_COLS];
                DataTablesSearch[] searchParams = new DataTablesSearch[MAX_COLS];

                foreach (var key in paramRequest.AllKeys)
                {
                    if (key.Equals("iDisplayStart"))
                        extractedParam.DisplayStart = paramRequest[key].ToInt32();
                    else if (key.Equals("iDisplayLength"))
                        extractedParam.DisplayLength = paramRequest[key].ToInt32();
                    else if (key.Equals("iColumns"))
                        extractedParam.Columns = paramRequest[key].ToInt32();
                    else if (key.Equals("sSearch"))
                        extractedParam.Search = paramRequest[key];
                    else if (key.Equals("bRegex"))
                        extractedParam.IsRegex = Convert.ToBoolean(paramRequest[key]);
                    else if (key.Equals("iSortingCols"))
                        extractedParam.SortingColumns = paramRequest[key].ToInt32();
                    else if (key.Equals("sEcho"))
                        extractedParam.Echo = paramRequest[key];
                    else if (key.Contains("bSearchable_"))
                    {
                        Int32 index = key.Replace("bSearchable_", "").ToInt32();
                        if (index < MAX_COLS)
                        {
                            if (searchParams[index] == null)
                                searchParams[index] = new DataTablesSearch();

                            searchParams[index].Column = index;
                            searchParams[index].Searchable = Convert.ToBoolean(paramRequest[key]);
                        }
                    }
                    else if (key.Contains("sSearch_"))
                    {
                        Int32 index = key.Replace("sSearch_", "").ToInt32();
                        if (index < MAX_COLS)
                        {
                            if (searchParams[index] == null)
                                searchParams[index] = new DataTablesSearch();

                            searchParams[index].Column = index;
                            searchParams[index].Search = paramRequest[key];
                        }
                    }
                    else if (key.Contains("bRegex_"))
                    {
                        Int32 index = key.Replace("bRegex_", "").ToInt32();
                        if (index < MAX_COLS)
                        {
                            if (searchParams[index] == null)
                                searchParams[index] = new DataTablesSearch();

                            searchParams[index].Column = index;
                            searchParams[index].Regex = Convert.ToBoolean(paramRequest[key]);
                        }
                    }
                    else if (key.Contains("iSortCol_"))
                    {
                        Int32 col = paramRequest[key].ToInt32();
                        if (sortParams[0] == null)
                        {
                            sortParams[0] = new DataTablesSort();
                        }

                        sortParams[0].Column = col;
                    }
                    else if (key.Contains("sSortDir_"))
                    {
                        Int32 index = key.Replace("sSortDir_", "").ToInt32();
                        if (sortParams[0] == null)
                        {
                            sortParams[0] = new DataTablesSort();
                        }
                        sortParams[0].SortDirection = paramRequest[key];
                    }
                    else if (key.Contains("mDataProp_"))
                    {
                        Int32 index = key.Replace("mDataProp_", "").ToInt32();
                        // do not know what should we do with this property
                    }
                }
                // Fill the return value
                for (Int32 curCol = 0; curCol < MAX_COLS; curCol++)
                {
                    if (searchParams[curCol] != null && searchParams[curCol].Column == curCol)
                        extractedParam.Searches.Add(searchParams[curCol]);

                }

                if (sortParams[0] != null)
                    extractedParam.Sorts.Add(sortParams[0]);

            }


            return extractedParam;
        }
    }
}
