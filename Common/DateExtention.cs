﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class DateExtention
    {
        public static bool IsWorkingDay(this DateTime date)
        {
            return
                date.DayOfWeek != DayOfWeek.Saturday &&
                date.DayOfWeek != DayOfWeek.Sunday;
        }
        public static int WorkingDaysTo(this DateTime fromDate, DateTime toDate)
        {
            int ret = 0;
            DateTime dt = fromDate;
            while (dt < toDate)
            {
                if (dt.IsWorkingDay()) ret++;
                dt = dt.AddDays(1);
            }
            return ret;
        }
    }
}
