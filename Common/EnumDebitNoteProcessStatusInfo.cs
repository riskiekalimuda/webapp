﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum EnumDebitNoteProcessStatusInfo
    {
        NONE = 0,
        CREATE_DN = 1,
        DN_VERIFICATION_APPROVED = 2,
        DN_VERIFICATION_REJECT = 3,
        RETURN_TO_CUSTOMER = 4,
        CHECKING_DN = 5, 
        REJECT_BY_APPROVER = 6,
        INTERNAL_ISSUE = 7,
        SALES_APPROVAL = 8,
        PROCESS_CN = 9,
        REJECT_BY_CNCREATOR = 10,
        CREATED_CN = 11
    }
}
