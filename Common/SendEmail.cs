﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class SendEmail
    {
        public static Boolean Send(String user, String pwd, Int32 port, String host, String from, String to, String subject, String msg)
        {
            Boolean result = false;
            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = port;
                client.Host = host;
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(user, pwd);

                MailMessage mm = new MailMessage(from, to, subject, msg);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);
                result = true;
            }
            catch (Exception ex)
            {
                //Do Nothing
            }
            return result;
        }
    }
}
