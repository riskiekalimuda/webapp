﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DataTablesSort
    {
        public Int32 Column { get; set; } // sorted column number
        public String SortDirection { get; set; } // the direction "desc" or "asc"
    }
}
