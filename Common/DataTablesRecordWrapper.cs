﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DataTablesRecordWrapper<T>
    {
        public Int32 iTotalRecords { get; set; }  // total keseluruhan record sebelum di filter
        public String sEcho { get; set; }
        public Int32 iTotalDisplayRecords { get; set; }  // total record setelah di filter
        public List<T> aaData { get; set; }
    }
}
