﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum EnumRole
    {
        NONE = 0,
        ADMINISTRATOR = 1,
        APPROVER = 2,
        REVIEWER2_CN_CREATOR = 3,
        REVIEWER1 = 4,
        PREPARER = 5
    }
}
