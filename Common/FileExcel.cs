﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Common
{
    public class FileExcel
    {
        public static List<T> ReadWorksheet<T>(String fileName, String worksheetName, String cellRange)
                where T : new()
        {
            List<T> result = new List<T>();

            if (!String.IsNullOrWhiteSpace(fileName) &&
                !String.IsNullOrWhiteSpace(worksheetName) &&
                !String.IsNullOrWhiteSpace(cellRange))
            {
                try
                {
                    if (File.Exists(fileName))
                    {
                        FileStream fs = new FileStream(fileName, FileMode.Open);
                        IWorkbook wBook = null;
                        ISheet wSheet = null;

                        String ext = Path.GetExtension(fileName);

                        if (ext.ToUpper().Equals(".XLSX"))
                            wBook = new XSSFWorkbook(fs);
                        else if (ext.ToUpper().Equals(".XLS"))
                            wBook = new HSSFWorkbook(fs);


                        if (wBook != null)
                        {
                            wSheet = wBook.GetSheet(worksheetName);

                            if (wSheet != null)
                            {
                                String[] arrCell = cellRange.Split(':');
                                Int32 startRow = 0;
                                Int32 endRow = 0;
                                Int32 startColumn = 0;
                                Int32 endColumn = 0;

                                //single cell
                                startColumn = GetColumnIndex(Regex.Replace(arrCell[0], @"[0-9\s]", String.Empty));
                                endColumn = startColumn;
                                startRow = Int32.Parse(Regex.Replace(arrCell[0], @"[A-Za-z\s]", String.Empty)) - 1;
                                endRow = startRow;

                                if (arrCell.Count() == 2)
                                {
                                    endColumn = GetColumnIndex(Regex.Replace(arrCell[1], @"[0-9\s]", String.Empty));
                                    endRow = Int32.Parse(Regex.Replace(arrCell[1], @"[A-Za-z\s]", String.Empty)) - 1;
                                }

                                Dictionary<String, Int32> mappedColumnToPropertyIndex = null;
                                ArrayList columnContents = new ArrayList();
                                for (var curRow = startRow; curRow <= endRow; curRow++)
                                {
                                    try
                                    {
                                        var row = wSheet.GetRow(curRow); //startRow = header

                                        if (row != null)
                                        {
                                            T objT = new T();

                                            for (var curCol = startColumn; curCol <= endColumn; curCol++)
                                            {
                                                String cellValue = "";
                                                if (row.GetCell(curCol) != null)
                                                {

                                                    switch (row.GetCell(curCol).CellType)
                                                    {
                                                        case CellType.Numeric:
                                                            try
                                                            {
                                                                String pattern = @"^[-+]?[0-9]*\.?[0-9]*$";
                                                                if (Regex.IsMatch(row.GetCell(curCol).ToString(), pattern)) {
                                                                    cellValue = row.GetCell(curCol).NumericCellValue.ToString();
                                                                }
                                                                else 
                                                                    cellValue = row.GetCell(curCol).ToString();
                                                            }
                                                            catch
                                                            {
                                                                cellValue = row.GetCell(curCol).NumericCellValue.ToString();
                                                            }
                                                            break;
                                                        case CellType.String:
                                                            cellValue = row.GetCell(curCol).StringCellValue;
                                                            break;
                                                        case CellType.Boolean:
                                                            cellValue = row.GetCell(curCol).BooleanCellValue.ToString();
                                                            break;
                                                        case CellType.Formula:
                                                            cellValue = row.GetCell(curCol).CellFormula;
                                                            break;
                                                        default:
                                                            break;
                                                    }



                                                    if (curRow != startRow && mappedColumnToPropertyIndex != null
                                                        && mappedColumnToPropertyIndex.ContainsKey(curCol.ToString()) && cellValue != null)
                                                    {
                                                        objT.GetType().GetProperties()[mappedColumnToPropertyIndex[curCol.ToString()]].SetValue(objT, cellValue);
                                                    }

                                                    if (curRow == startRow)
                                                    {
                                                        columnContents.Add(cellValue);
                                                    }
                                                }

                                            }

                                            if (curRow == startRow)
                                            {
                                                mappedColumnToPropertyIndex = MapHeaderToPropertyIndex(objT, columnContents);
                                            }
                                            else
                                            {
                                                result.Add(objT);
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        throw;
                                    }

                                } // end for
                            } // end if wSheet != null
                        } // end if wBook != null
                    } // end if file exist
                }
                catch
                {
                    throw;
                }
            }


            return result;
        }

        /// <summary>
        /// Convert column name (in alphabet) into column number
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        private static Int32 GetColumnIndex(String columnName)
        {
            Int32 result = 0;

            Int32 columnNameLength = columnName.Length;

            if (!String.IsNullOrWhiteSpace(columnName))
            {
                for (Int32 i = 0; i < columnNameLength; i++)
                {
                    if (i == 0)
                        result += (Convert.ToByte(columnName[columnNameLength - i - 1]) - 64) - 1;
                    else
                        result += 26 * i * (Convert.ToByte(columnName[columnNameLength - i - 1]) - 64);
                }
            }

            return result;
        }
        /// <summary>
        /// MapHeaderToPropertyIndex
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="colNames"></param>
        /// <returns></returns>
        private static Dictionary<String, Int32> MapHeaderToPropertyIndex(Object obj, ArrayList colNames)
        {
            Dictionary<String, Int32> result = new Dictionary<String, Int32>();

            for (var i = 0; i < colNames.Count; i++)
            {
                for (var j = 0; j < obj.GetType().GetProperties().Count(); j++)  // loop for all property inside the object
                {
                    String colName = colNames[i].ToString().Replace(" ", String.Empty);

                    if (colName.ToUpper().Equals(obj.GetType().GetProperties()[j].Name.ToUpper()))
                    {
                        result.Add(j.ToString(), j);
                        break;
                    }
                }
            }

            return result;
        }
    }
}
