﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class HttpReturnResult
    {
         public Int64 IdRowData { get; set; }
        public Int16 FrmtPrint { get; set; }
        public Int16 SuccessfulProcess { get; set; }
        public NotificationMessage ResultMessage { get; set; }
    }
}
