﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum EnumEmployeeType
    {
        NONE = 0,
        INTERNAL = 1,
        EXTERNAL = 2
    }
}
