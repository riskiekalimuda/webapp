﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MMI.Common;

namespace Inventory.Web.Extention
{
    public class UserDetail
    {
        public String Role { get; set; }
        public Int64 UserPK { get; set; }
        public Int64 EmpPK { get; set; }
    }
    public static class ExtentionHelper
    {
        public static UserDetail GetRoleFromCookie(this Controller controller)
        {
            UserDetail user = null;

            String cookie = FormsAuthentication.Decrypt(controller.Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;

            if (!String.IsNullOrWhiteSpace(cookie))
            {
                user = new UserDetail();
                user.Role = cookie.Split('|')[0].ToString();
                user.UserPK = cookie.Split('|')[1].ToInt64();
                user.EmpPK = cookie.Split('|')[2].ToInt64();
            }
            return user;
        }
    }
}