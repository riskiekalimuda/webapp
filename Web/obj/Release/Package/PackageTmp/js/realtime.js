﻿$(document).ready(function () {
    startTime();
});

function startTime() {
    var today = new Date();
    var formatted = $.datepicker.formatDate("d-M-yy", today);
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('livetime').innerHTML = formatted + "&nbsp;&nbsp;&nbsp;&nbsp;" +
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
    return i;
}