﻿function showAlertMessage(placeholder, title, text, type) {
    $(placeholder).append(
    '<div class="alert ' + type + ' alert-block">' +
                '<a class="close" data-dismiss="alert" href="#">×</a>' +
                '<h4 class="alert-heading"><i class="fa fa-check-square-o"></i>' + title + '</h4>' +
                '<p>' +
                    text +
                '</p>' +
            '</div>');
};