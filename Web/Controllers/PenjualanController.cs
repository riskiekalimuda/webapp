﻿using Common;
using KaryawanORM;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Web.Controllers
{
    [Authorize]
    public class PenjualanController : Controller
    {
        //
        // GET: /Penjualan/
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List()
        {
            DataTablesPostParam param = DataTablesPostParam.ExtractRequestParams(Request.Params);
            DataTablesRecordWrapper<TransaksiPenjualanGrid> result = new DataTablesRecordWrapper<TransaksiPenjualanGrid>();

            result = TransaksiPenjualan.GridSearchPenjualan(param);

            return new JsonResult { Data = result };
        }

        public ActionResult Create()
        {
            BindDataProduk();
            BindDataKaryawan();
            return View();
        }

        [HttpGet]
        public ActionResult Edit(Int64? id)
        {
            BindDataKaryawan();
            BindDataProduk();

            TransaksiPenjualanDetail data = TransaksiPenjualan.GetPenjualanDetailByPenjualanID(id.Value);
            return View(data);
        }

        [HttpPost]
        public ActionResult Edit(TransaksiPenjualanDetail penjualanDetail)
        {
            HttpReturnResult result = new HttpReturnResult();
            List<PenjualanItemDetailLookUp> lstPenjualanItems = new List<PenjualanItemDetailLookUp>();
            if (penjualanDetail != null)
            {
                if (!String.IsNullOrWhiteSpace(penjualanDetail.PenjualanItemJson))
                    lstPenjualanItems = PenjualanItemDetailLookUp.ConvertPenjualanItemFromJson(penjualanDetail.PenjualanItemJson);

                TrxPenjualan newPenjualan = new TrxPenjualan();
                newPenjualan.PenjualanID = penjualanDetail.PenjualanID;
                newPenjualan.PenjualanTanggal = penjualanDetail.PenjualanTanggal;
                newPenjualan.PenjualanKaryawanID = penjualanDetail.PenjualanKaryawanID;
                newPenjualan.PenjualanGrandTotal = penjualanDetail.PenjualanGrandTotal;
                if (TransaksiPenjualan.Update(newPenjualan) > 0)
                {
                    //Hapus semua penjualan item
                    TransaksiPenjualanItem.DeletePenjualanItemByPenjualanID(newPenjualan.PenjualanID);

                    result.IdRowData = newPenjualan.PenjualanID;
                    result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format(Constants.SAVE_SUCCEEDED_DESCRIPTION, "Penjualan"), EnumNotificationMessageType.ALERT_SUCCESS);
                    if (lstPenjualanItems != null && lstPenjualanItems.Count() > 0)
                    {
                        foreach (var item in lstPenjualanItems)
                        {
                            TrxPenjualanItem penjualanItem = new TrxPenjualanItem();
                            penjualanItem.PenjualanItemPenjualanID = newPenjualan.PenjualanID;
                            penjualanItem.PenjualanItemProdukID = item.PenjualanItemProdukID;
                            penjualanItem.PenjualanItemJumlah = item.PenjualanItemJumlah;
                            penjualanItem.PenjualanItemHarga = item.PenjualanItemHarga;
                            penjualanItem.PenjualanItemTotal = item.PenjualanItemTotal;
                            TransaksiPenjualanItem.Insert(penjualanItem);
                        }
                    }
                }
                else
                    result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Penjualan"), EnumNotificationMessageType.ALERT_FAILED);
            }
            return this.Json(result);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(Int64 id)
        {
            NotificationMessage result = new NotificationMessage();

            if (id > 0)
            {
                if (TransaksiPenjualan.DeletePenjualanDetail(id) > 0)
                    result = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format(Constants.DELETE_SUCCEEDED_DESCRIPTION, "Penjualan"), EnumNotificationMessageType.ALERT_SUCCESS);
                else
                    result = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.DELETE_FAILED_DESCRIPTION, "Penjualan"), EnumNotificationMessageType.ALERT_FAILED);
            }

            return this.Json(result);
        }

        [HttpPost]
        public ActionResult Create(TransaksiPenjualanDetail penjualanDetail)
        {
            HttpReturnResult result = new HttpReturnResult();
            List<PenjualanItemDetailLookUp> lstPenjualanItems = new List<PenjualanItemDetailLookUp>();
            if (penjualanDetail != null)
            {
                if (!String.IsNullOrWhiteSpace(penjualanDetail.PenjualanItemJson))
                    lstPenjualanItems = PenjualanItemDetailLookUp.ConvertPenjualanItemFromJson(penjualanDetail.PenjualanItemJson);

                TrxPenjualan newPenjualan = new TrxPenjualan();
                newPenjualan.PenjualanTanggal = penjualanDetail.PenjualanTanggal;
                newPenjualan.PenjualanKaryawanID = penjualanDetail.PenjualanKaryawanID;
                newPenjualan.PenjualanGrandTotal = penjualanDetail.PenjualanGrandTotal;
                if (TransaksiPenjualan.Insert(newPenjualan) > 0)
                {
                    result.IdRowData = newPenjualan.PenjualanID;
                    result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format(Constants.SAVE_SUCCEEDED_DESCRIPTION, "Penjualan"), EnumNotificationMessageType.ALERT_SUCCESS);
                    if (lstPenjualanItems != null && lstPenjualanItems.Count() > 0)
                    {
                        foreach (var item in lstPenjualanItems)
                        {
                            TrxPenjualanItem penjualanItem = new TrxPenjualanItem();
                            penjualanItem.PenjualanItemPenjualanID = newPenjualan.PenjualanID;
                            penjualanItem.PenjualanItemProdukID = item.PenjualanItemProdukID;
                            penjualanItem.PenjualanItemJumlah = item.PenjualanItemJumlah;
                            penjualanItem.PenjualanItemHarga = item.PenjualanItemHarga;
                            penjualanItem.PenjualanItemTotal = item.PenjualanItemTotal;
                            TransaksiPenjualanItem.Insert(penjualanItem);
                        }
                    }
                }
                else
                    result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Penjualan"), EnumNotificationMessageType.ALERT_FAILED);
            }
            return this.Json(result);
        }

        public void BindDataProduk()
        {
            List<ProdukDetail> lstProduk = MasterProduk.GetAllProdukDetails();
            if (lstProduk == null)
                lstProduk = new List<ProdukDetail>();

            ViewBag.Produks = new SelectList(lstProduk, "ProdukID", "ProdukNamaFormat");
        }

        public void BindDataKaryawan()
        {
            List<MstKaryawan> lstKaryawan = MasterKaryawan.GetAllKaryawans();
            if (lstKaryawan == null)
                lstKaryawan = new List<MstKaryawan>();

            ViewBag.Karyawans = new SelectList(lstKaryawan, "KaryawanID", "KaryawanNama");
        }

    }
}
