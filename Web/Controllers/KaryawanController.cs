﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MMI.Common;
using KaryawanORM.LookUp;
using KaryawanORM;
using System.IO;
using Common;
using Microsoft.Reporting.WebForms;
using System.Text.RegularExpressions;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System.Drawing;
using System.Net.Configuration;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace Inventory.Web.Controllers
{
    [Authorize]
    public class KaryawanController : Controller
    {
        //
        // GET: /Karyawan/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendMail()
        {
            NotificationMessage result = new NotificationMessage();
            try
            {
                string textBody = "<table border=" + 1 + " cellpadding=" + 0 + " cellspacing=" + 0 + " width = " + 400 + "><tr bgcolor='#4da6ff'><td><b>KODE</b></td> <td> <b>NAMA </b> </td><td> <b>ALAMAT </b> </td><td> <b>TELP. </b> </td></tr>";
                

                List<MstKaryawan>lstKaryawans = MasterKaryawan.GetAllKaryawans();
                if (lstKaryawans != null && lstKaryawans.Count() > 0)
                {
                    foreach (var item in lstKaryawans)
                    {
                        textBody += "<tr><td>" + item.KaryawanKode + "</td><td>" + item.KaryawanNama + "</td><td>" + item.KaryawanAlamat + "</td><td>" + item.KaryawanTelp + "</td></tr>";
                    }
                }
                textBody += "</table>";
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress("riskie.kalimuda@gmail.com");
                message.To.Add(new MailAddress("mr.cell09@gmail.com"));
                message.Subject = "Daftar Karyawan";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = "Berikut ini adalah daftar nama-nama karyawan yang diambil dari sisteim" + Environment.NewLine+
                    textBody;
                smtp.Port = 587;
                smtp.Host = "smtp.gmail.com"; //for gmail host  
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("riskie.kalimuda@gmail.com", "marthaak0309258046");
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
                result = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format("Kirim email berhasil", "Send Mail"), EnumNotificationMessageType.ALERT_SUCCESS);
            }
            catch (Exception ex)
            {
                result = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format("Kirim email gagal", "Send Mail"), EnumNotificationMessageType.ALERT_FAILED);
            }
            return this.Json(result);
        }

        [HttpPost]
        public ActionResult List()
        {
            DataTablesPostParam param = DataTablesPostParam.ExtractRequestParams(Request.Params);
            DataTablesRecordWrapper<MasterKaryawanGrid> result = new DataTablesRecordWrapper<MasterKaryawanGrid>();
            result = MasterKaryawan.GridSearchEntity(param);
            return new JsonResult { Data = result };
        }

        [HttpGet]
        public void ExportPDF()
        {
            //Create an instance of PdfDocument.
            PdfDocument document = new PdfDocument();
                //Add a page to the document
                PdfPage page = document.Pages.Add();

                //Create PDF graphics for the page
                PdfGraphics graphics = page.Graphics;

                //Set the standard font
                PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);

                //Draw the text
                graphics.DrawString("Hello World!!!", font, PdfBrushes.Black, new PointF(0, 0));

                // Open the document in browser after saving it
                document.Save("Output.pdf", HttpContext.ApplicationInstance.Response, HttpReadType.Save);
            }


        [HttpPost]
        public ActionResult GenerateDaftarToPdf()
        {
            List<ExportPdfLookUp> lstExportResult = null;
            lstExportResult = MasterKaryawan.GenerateExportPdf();
          
            if(lstExportResult == null)
                lstExportResult = new List<ExportPdfLookUp>();

                LocalReport localReport = new LocalReport();
                string path = Path.Combine(Server.MapPath("~/Rdlc"), "Exportpdf.rdlc");
                if (System.IO.File.Exists(path))
                {
                    localReport.ReportPath = path;
                }
                else
                {
                    return null;
                }

                ReportDataSource ds = new ReportDataSource("dsExportPdf", lstExportResult);
                localReport.DataSources.Add(ds);

                String mimeType;
                String encoding;
                String fileNameExtension;

                ReportPageSettings setting = localReport.GetDefaultPageSettings();


                string deviceInfo =

                "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "  <PageWidth>" + setting.PaperSize.Width + "in</PageWidth>" +
                "  <PageHeight>" + setting.PaperSize.Height + "in</PageHeight>" +
                "  <MarginTop>" + setting.Margins.Top + "in</MarginTop>" +
                "  <MarginLeft>" + setting.Margins.Left + "in</MarginLeft>" +
                "  <MarginRight>" + setting.Margins.Right + "in</MarginRight>" +
                "  <MarginBottom>" + setting.Margins.Bottom + "in</MarginBottom>" +
                "</DeviceInfo>";

                Warning[] warnings;
                String[] streams;
                Byte[] renderedBytes;

                renderedBytes = localReport.Render(
                    "PDF",
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);


                String data = Convert.ToBase64String(renderedBytes);
                return Json(data);
        }

        [HttpPost]
        public ActionResult GenerateDaftarToExcel()
        {
            List<ExportPdfLookUp> lstExportResult = null;
            lstExportResult = MasterKaryawan.GenerateExportPdf();

            if (lstExportResult == null)
                lstExportResult = new List<ExportPdfLookUp>();

            LocalReport localReport = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Rdlc"), "Exportpdf.rdlc");
            if (System.IO.File.Exists(path))
            {
                localReport.ReportPath = path;
            }
            else
            {
                return null;
            }

            ReportDataSource ds = new ReportDataSource("dsExportPdf", lstExportResult);
            localReport.DataSources.Add(ds);

            String mimeType;
            String encoding;
            String fileNameExtension;

            ReportPageSettings setting = localReport.GetDefaultPageSettings();
            
            string deviceInfo =
            "<DeviceInfo>" +
            "  <SimplePageHeaders>False</SimplePageHeaders>" +
            "</DeviceInfo>";

            Warning[] warnings;
            String[] streams;
            Byte[] renderedBytes;

            renderedBytes = localReport.Render(
                "EXCEL",
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            String data = Convert.ToBase64String(renderedBytes);
            return Json(data);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult Delete(Int64 id)
        {
            NotificationMessage result = new NotificationMessage();

            if (id > 0)
            {
                if (MasterKaryawan.Delete(id) > 0)
                {
                    result = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format(Constants.DELETE_SUCCEEDED_DESCRIPTION, "Data Karyawan"), EnumNotificationMessageType.ALERT_SUCCESS);
                }
                else
                {
                    result = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.DELETE_FAILED_DESCRIPTION, "Data Karyawan"), EnumNotificationMessageType.ALERT_FAILED);
                }

            }
            return this.Json(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LookUpKaryawan karyawan)
        {
            String result = "";
            Boolean cekExtentionFotoResult = true;
            MstKaryawan newKaryawan = new MstKaryawan();
            String formatKode = karyawan.KaryawanKode.ToUpper();

            if (!MasterKaryawan.CekKaryawanByKode(formatKode))
            {
                if (karyawan.KaryawanFoto != null)
                {
                    String fileName = Guid.NewGuid().ToString() + karyawan.KaryawanFoto.FileName;
                    String frmtExt = System.IO.Path.GetExtension(fileName);
                    if (MasterKaryawan.CekFormatExtensiFoto(frmtExt))
                    {
                        String uploadFolder = Server.MapPath("~/Uploads/ID/");
                        Boolean isDestPathExist = Directory.Exists(uploadFolder);

                        if (!isDestPathExist)
                            Directory.CreateDirectory(uploadFolder);

                        String savedFileName = uploadFolder + "\\" + fileName;
                        karyawan.KaryawanFoto.SaveAs(savedFileName);
                        newKaryawan.KaryawanFoto = "/Uploads/ID/" + fileName;
                    }
                    else
                    {
                        result = String.Format(Constants.SAVE_FAILED_DESCRIPTION, "File yg diupload bukan gambar");
                        cekExtentionFotoResult = false;
                    }
            
                }
                if (cekExtentionFotoResult)
                {
                    newKaryawan.KaryawanKode = karyawan.KaryawanKode;
                    newKaryawan.KaryawanNama = karyawan.KaryawanNama;
                    newKaryawan.KaryawanAlamat = karyawan.KaryawanAlamat;
                    newKaryawan.KaryawanTelp = karyawan.KaryawanTelp;
                    if (MasterKaryawan.Insert(newKaryawan) > 0)
                        result = String.Format(Constants.SAVE_SUCCEEDED_DESCRIPTION, "Data Karyawan");
                    else
                        result = String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Data Karyawan");
                }
            }
            else
                result = String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Data Karyawan Sudah Ada");
            
            return this.Json(result);
        }

        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            LookUpKaryawan lookKry = new LookUpKaryawan();
            if (id > 0)
            {
                MstKaryawan ambilKry = MasterKaryawan.GetKaryawanByID(id);
                if (ambilKry != null)
                {
                    lookKry.KaryawanID = ambilKry.KaryawanID;
                    lookKry.KaryawanKode = ambilKry.KaryawanKode;
                    lookKry.KaryawanNama = ambilKry.KaryawanNama;
                    lookKry.KaryawanAlamat = ambilKry.KaryawanAlamat;
                    lookKry.KaryawanTelp = ambilKry.KaryawanTelp;
                    lookKry.KaryawanFotoFileName = ambilKry.KaryawanFoto;
                }
            }
            return View(lookKry);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LookUpKaryawan karyawan)
        {
            HttpReturnResult result = new HttpReturnResult();
            String formatKode = karyawan.KaryawanKode.ToUpper();
            Boolean cekFotoFormat = true;
            MstKaryawan newKaryawan = new MstKaryawan();
            if (ModelState.IsValid)
            {
                if (!MasterKaryawan.IsDataExist(karyawan.KaryawanID, formatKode))
                {
                    if (karyawan.KaryawanFoto != null)
                    {
                        String fileName = Guid.NewGuid().ToString() + karyawan.KaryawanFoto.FileName;
                        String selectedName = Path.GetExtension(fileName);
                        if (MasterKaryawan.CekFormatExtensiFoto(selectedName))
                        {
                            String uploadFolder = Server.MapPath("~/Uploads/ID/");
                            Boolean isDestPathExist = Directory.Exists(uploadFolder);

                            if (!isDestPathExist)
                            {
                                Directory.CreateDirectory(uploadFolder);
                            }

                            String savedFileName = uploadFolder + "\\" + fileName;
                            karyawan.KaryawanFoto.SaveAs(savedFileName);
                            newKaryawan.KaryawanFoto = "/Uploads/ID/" + fileName;
                        }
                        else
                        {
                            cekFotoFormat = false;
                            result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Bukan foto"), EnumNotificationMessageType.ALERT_FAILED);
                        }
                        
                    }
                    else
                    {
                        newKaryawan.KaryawanFoto = karyawan.KaryawanFotoFileName;
                    }
                    if (cekFotoFormat)
                    {
                        newKaryawan.KaryawanID = karyawan.KaryawanID;
                        newKaryawan.KaryawanKode = karyawan.KaryawanKode;
                        newKaryawan.KaryawanNama = karyawan.KaryawanNama;
                        newKaryawan.KaryawanAlamat = karyawan.KaryawanAlamat;
                        newKaryawan.KaryawanTelp = karyawan.KaryawanTelp;
                        if (MasterKaryawan.Update(newKaryawan) > 0)
                            result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format(Constants.SAVE_SUCCEEDED_DESCRIPTION, "Karyawan"), EnumNotificationMessageType.ALERT_SUCCESS);
                        else
                            result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Karyawan"), EnumNotificationMessageType.ALERT_FAILED);
                    }
                }
                else
                {
                    result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Karyawan Sudah Ada"), EnumNotificationMessageType.ALERT_FAILED);
                }
            }
            else
                result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Server error"), EnumNotificationMessageType.ALERT_FAILED);
            
            return this.Json(result);
        }
    }
}
