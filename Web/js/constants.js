﻿var SimKKP = (SimKKP == null) ? {} : SimKKP;

SimKKP.Constants = {
    DONEPROCESS_TITLE: 'Customer Order Will Be Done',
    DONEPROCESS_CONFIRMATION: 'Apakah Anda yakin untuk jadikan status pesanan menjadi selesai?',
    NONAKTIF_TITLE: 'Non Aktifkan Data',
    NONAKTIF_CONFIRMATION: 'Apakah Anda yakin untuk menonaktifkan data ini?',
    AKTIF_TITLE: 'Aktifkan Data',
    AKTIF_CONFIRMATION: 'Apakah Anda yakin untuk mengaktifkan data ini?',
    DELETE_TITLE: 'Hapus Data',
    DELETE_ALL_TITLE: 'Hapus Semua Data',
    DELETE_CONFIRMATION: 'Apakah Anda yakin untuk menghapus data ini?',
    STOCK_TITLE: 'Informasi Stock',
    STOCK_NOT_AVAILABLE:'Stock tidak tersedia'
};