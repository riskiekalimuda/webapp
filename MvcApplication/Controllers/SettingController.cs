﻿using KaryawanORM;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Controllers
{
    public class SettingController : Controller
    {
        //
        // GET: /Setting/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAllSetting()
        {
            List<MasterSettingGrid> result = MasterSetting.GetAllSettingForGrid();
            return this.Json(result);
        }

    }
}
