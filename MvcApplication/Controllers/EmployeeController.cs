﻿using Common;
using KaryawanORM;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        //
        // GET: /Employee/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetEmployees()
        {
            var lstKaryawans = MasterKaryawan.GetAllKaryawansForGrid();
            return this.Json(lstKaryawans);
        }

        public ActionResult AmbilData()
        {
            ViewBag.Karyawans = MasterKaryawan.GetAllKaryawansForGrid();
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LookUpKaryawan karyawan)
        {
            String result = "";
            Boolean cekExtentionFotoResult = true;
            MstKaryawan newKaryawan = new MstKaryawan();
            String formatKode = karyawan.KaryawanKode.ToUpper();

            if (!MasterKaryawan.CekKaryawanByKode(formatKode))
            {
                if (karyawan.KaryawanFoto != null)
                {
                    String fileName = Guid.NewGuid().ToString() + karyawan.KaryawanFoto.FileName;
                    String frmtExt = System.IO.Path.GetExtension(fileName);
                    if (MasterKaryawan.CekFormatExtensiFoto(frmtExt))
                    {
                        String uploadFolder = Server.MapPath("~/Uploads/ID/");
                        Boolean isDestPathExist = Directory.Exists(uploadFolder);

                        if (!isDestPathExist)
                            Directory.CreateDirectory(uploadFolder);

                        String savedFileName = uploadFolder + "\\" + fileName;
                        karyawan.KaryawanFoto.SaveAs(savedFileName);
                        newKaryawan.KaryawanFoto = "/Uploads/ID/" + fileName;
                    }
                    else
                    {
                        result = String.Format(Constants.SAVE_FAILED_DESCRIPTION, "File yg diupload bukan gambar");
                        cekExtentionFotoResult = false;
                    }

                }
                if (cekExtentionFotoResult)
                {
                    newKaryawan.KaryawanKode = karyawan.KaryawanKode;
                    newKaryawan.KaryawanNama = karyawan.KaryawanNama;
                    newKaryawan.KaryawanAlamat = karyawan.KaryawanAlamat;
                    newKaryawan.KaryawanTelp = karyawan.KaryawanTelp;
                    if (MasterKaryawan.Insert(newKaryawan) > 0)
                        result = String.Format(Constants.SAVE_SUCCEEDED_DESCRIPTION, "Data Karyawan");
                    else
                        result = String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Data Karyawan");
                }
            }
            else
                result = String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Data Karyawan Sudah Ada");

            return this.Json(result);
        }

        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            LookUpKaryawan lookKry = new LookUpKaryawan();
            if (id > 0)
            {
                MstKaryawan ambilKry = MasterKaryawan.GetKaryawanByID(id);
                if (ambilKry != null)
                {
                    lookKry.KaryawanID = ambilKry.KaryawanID;
                    lookKry.KaryawanKode = ambilKry.KaryawanKode;
                    lookKry.KaryawanNama = ambilKry.KaryawanNama;
                    lookKry.KaryawanAlamat = ambilKry.KaryawanAlamat;
                    lookKry.KaryawanTelp = ambilKry.KaryawanTelp;
                    lookKry.KaryawanFotoFileName = ambilKry.KaryawanFoto;
                }
            }
            return View(lookKry);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LookUpKaryawan karyawan)
        {
            HttpReturnResult result = new HttpReturnResult();
            String formatKode = karyawan.KaryawanKode.ToUpper();
            Boolean cekFotoFormat = true;
            MstKaryawan newKaryawan = new MstKaryawan();
            if (ModelState.IsValid)
            {
                if (!MasterKaryawan.IsDataExist(karyawan.KaryawanID, formatKode))
                {
                    if (karyawan.KaryawanFoto != null)
                    {
                        String fileName = Guid.NewGuid().ToString() + karyawan.KaryawanFoto.FileName;
                        String selectedName = Path.GetExtension(fileName);
                        if (MasterKaryawan.CekFormatExtensiFoto(selectedName))
                        {
                            String uploadFolder = Server.MapPath("~/Uploads/ID/");
                            Boolean isDestPathExist = Directory.Exists(uploadFolder);

                            if (!isDestPathExist)
                            {
                                Directory.CreateDirectory(uploadFolder);
                            }

                            String savedFileName = uploadFolder + "\\" + fileName;
                            karyawan.KaryawanFoto.SaveAs(savedFileName);
                            newKaryawan.KaryawanFoto = "/Uploads/ID/" + fileName;
                        }
                        else
                        {
                            cekFotoFormat = false;
                            result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Bukan foto"), EnumNotificationMessageType.ALERT_FAILED);
                        }

                    }
                    else
                    {
                        newKaryawan.KaryawanFoto = karyawan.KaryawanFotoFileName;
                    }
                    if (cekFotoFormat)
                    {
                        newKaryawan.KaryawanID = karyawan.KaryawanID;
                        newKaryawan.KaryawanKode = karyawan.KaryawanKode;
                        newKaryawan.KaryawanNama = karyawan.KaryawanNama;
                        newKaryawan.KaryawanAlamat = karyawan.KaryawanAlamat;
                        newKaryawan.KaryawanTelp = karyawan.KaryawanTelp;
                        if (MasterKaryawan.Update(newKaryawan) > 0)
                            result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format(Constants.SAVE_SUCCEEDED_DESCRIPTION, "Karyawan"), EnumNotificationMessageType.ALERT_SUCCESS);
                        else
                            result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Karyawan"), EnumNotificationMessageType.ALERT_FAILED);
                    }
                }
                else
                {
                    result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Karyawan Sudah Ada"), EnumNotificationMessageType.ALERT_FAILED);
                }
            }
            else
                result.ResultMessage = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.SAVE_FAILED_DESCRIPTION, "Server error"), EnumNotificationMessageType.ALERT_FAILED);

            return this.Json(result);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult Delete(Int64 id)
        {
            NotificationMessage result = new NotificationMessage();

            if (id > 0)
            {
                if (MasterKaryawan.Delete(id) > 0)
                {
                    result = NotificationMessage.Compose(Constants.ACTION_SUCCEEDED_TITLE, String.Format(Constants.DELETE_SUCCEEDED_DESCRIPTION, "Data Karyawan"), EnumNotificationMessageType.ALERT_SUCCESS);
                }
                else
                {
                    result = NotificationMessage.Compose(Constants.ACTION_FAILED_TITLE, String.Format(Constants.DELETE_FAILED_DESCRIPTION, "Data Karyawan"), EnumNotificationMessageType.ALERT_FAILED);
                }

            }
            return this.Json(result);
        }
    }
}
