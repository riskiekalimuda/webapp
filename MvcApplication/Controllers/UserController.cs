﻿using KaryawanORM;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Controllers
{
  [Authorize]
    public class UserController : Controller
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAllUsers()
        {
            List<MasterUsersGrid> result = MasterUser.GetAllUsers();
            return this.Json(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterUserLookUp newUser)
        {
            Boolean result = false;
            if (MasterUser.InsertUserByLookUp(newUser) > 0)
                result = true;
          
            return this.Json(result);
        }

        [HttpGet]
        public ActionResult Edit(Int64 id)
        {
            MasterUserLookUp result = new MasterUserLookUp();
            result = MasterUser.GetUserByUserLookUp(id);
            return View(result);
        }

        [HttpPost]
        public ActionResult Edit(MasterUserLookUp userLookUp)
        {
            Boolean result = false;
            Int32 insertResult = MasterUser.InsertUserByLookUp(userLookUp);
            if (insertResult > 0)
                result = true;

            return this.Json(result);
        }

        [HttpPost]
        public ActionResult Delete(Int64 id)
        {
            Boolean result = false;
            if (MasterUser.Delete(id) > 0)
                result = true;
            return this.Json(result);
        }
    }
}
