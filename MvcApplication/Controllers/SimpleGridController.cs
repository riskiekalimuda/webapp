﻿using KaryawanORM;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication.Controllers
{
    public class SimpleGridController : Controller
    {
        //
        // GET: /SimpleGrid/

        public ActionResult Index()
        {
            List<MasterKaryawanGrid>lstData = MasterKaryawan.GetAllKaryawansForGrid();
            return View(lstData);
        }

    }
}
