﻿using KaryawanORM;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

namespace MvcApplication.Controllers
{
    public class AuthenticateController : Controller
    {
        //
        // GET: /Authenticate/

        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult Login(String returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(ParamLogin paramLogin, String returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (paramLogin != null)
                {
                    if (MasterUser.IsUserHaveLogin(paramLogin.Username, paramLogin.Password))
                    {
                        FormsAuthentication.SetAuthCookie(paramLogin.Username, false);
                    }
                    return RedirectToHomePage(returnUrl);
                }
            }
            return View(paramLogin);
        }

        public ActionResult RedirectToHomePage(String returnUrl)
        {
            String decodedUrl = Server.UrlDecode(returnUrl);
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}
