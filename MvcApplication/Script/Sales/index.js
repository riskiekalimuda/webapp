﻿$(document).ready(function () {
    $.ajax({
        url: '/Sales/GetAllSalesForGrid',
        method: 'post',
        dataType: 'json',
        success: function (data) {
            $('#tblSales').dataTable({
                data: data,
                order: [[0, 'desc']],
                columns: [
                    { data: 'PenjualanID', visible: false },
                    { data: 'PenjualanTanggalFormat', width: '100px' },
                    { data: 'PenjualanKaryawanNama', width: '200px' },
                    { data: 'PenjualanGrandTotalFormat', width: '150px' },
                    {
                        data: function (data, type, full, meta) {
                            return '<a class="btn btn-warning" href="/Sales/Edit/'+ data.PenjualanID +'">Edit</a>';
                        }, width:'25px'
                    },
                    {
                        data: function (data, type, full, meta) {
                            return '<a class="delete btn btn-danger">Delete</a>';
                        }, width:'30px'
                    }
                    ]
            });
        },
        error: function (e) {
            alert(e);
        }
    });

    $('#demo-dt-addrow-btn').on('click', function () {
        window.location = "/Sales/Create";
    });

   
    $('#tblSales').on('click', 'tr a.delete', function (e) {
        e.preventDefault();
        var t = $('#tblSales').DataTable();
        var selectedRow = $(this).parents('tr')[0];
        var selectedData = t.row(selectedRow).data();
        var deleteResult = false;

        //konfirmasi delete
        var konfirmDelete = confirm("Yakin data akan dihapus?");
        if (konfirmDelete) {
            $.ajax({
                url: '/Sales/Delete',
                method: 'post',
                async:false,
                data: { id: selectedData.PenjualanID },
                success: function (e) {
                    deleteResult = true;
                },
                error: function (e) {
                    alert("Gagal hapus data!");
                }
            });
        }

        if (deleteResult) {
            t.row(selectedRow).remove().draw();
        }
    });
});