﻿$(document).ready(function () {
    var formEditSales = $('#formEditSales').validate({
        rules: {
            PenjualanTanggal: {
                required: true
            },
            PenjualanKaryawanID: {
                required:true
            }
        },
        messages: {
            PenjualanTanggal: {
                required: '-(HARUS DIISI)'
            },
            PenjualanKaryawanID: {
                required: '-(HARUS DIISI)'
            }
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            $(element).siblings("label").append(error);
        },
        highlight: function (element) {
            $(element).siblings("label").addClass("error");
        },
        unhighlight: function(element){
            $(element).siblings("label").removeClass("error");
        },
        submitHandler: function (form) {
            btn_Save(form);
            return false;
        }
    });

    //Assign select2 plug in
    $('.select2').select2();

    $('#PenjualanTanggal').datepicker(
        {
            dateFormat: 'dd MM yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>'
        });

    //set format tanggal terpilih
    var tanggalPenjualan = $("#PenjualanTanggal").val();
    if (tanggalPenjualan != "")
        $("#PenjualanTanggal").val(moment(tanggalPenjualan).format("DD MMMM YYYY"));

    //set grand total
    var gT = "Rp." + $('#PenjualanGrandTotal').val();
    $('#txtGrandTotal').val(gT);


    var oTable = $('#dt_itemlist').dataTable({
        columns: [
            { data: 'PenjualanItemProdukID', visible: false },
            { data: 'PenjualanItemProduk', width: '100px' },
            { data: 'PenjualanItemJumlah', width: '100px' },
            { data: 'PenjualanItemHarga', width: '100px' },
            { data: 'PenjualanItemTotal', width: '100px' },
            {
                data: function () {
                    return '<a class="delete btn btn-danger" >Delete</a>';
                }
            }
        ]
    });

    //bind data to items grid
    var t = $('#dt_itemlist').DataTable();
    var itemsSource = $('#PenjualanItemJson').val();
    if (itemsSource != "") {
        var items = JSON.parse(itemsSource);
        $.each(items, function (id, value) {
            t.row.add({
                'PenjualanItemProdukID': value.PenjualanItemProdukID,
                'PenjualanItemProduk': value.PenjualanItemProduk,
                'PenjualanItemJumlah': value.PenjualanItemJumlah,
                'PenjualanItemHarga': value.PenjualanItemHarga,
                'PenjualanItemTotal': value.PenjualanItemTotal
            }).draw();
        });
    }
    //var items = 

    $('#dt_itemlist').on('click', 'tr', function () {
        var t = $('#dt_itemlist').DataTable();
        //Hitung grand total
        var selectedDataDelete = t.row(this).data();
        var total = (selectedDataDelete.PenjualanItemTotal).replace("Rp.", "");
        var grandTotal = $('#txtGrandTotal').val().replace("Rp.", "");
        var finalGT = "Rp." + (Number(grandTotal) - Number(total));
        $('#txtGrandTotal').val(finalGT);

        //delete row
        t.row(this).remove().draw();
    });

    $('#ddlItemName').on('change', function (e) {
        var selected = $('option:selected', this);
        var selectedValue = selected.val();
        var selectedText = selected.text();
        var harga = selectedText.split('\n')[1];
        var namaProduk = selectedText.split('\n')[0];
        $("#txtHarga").val(harga);
        $('#produkID').val(selectedValue);
        $('#namaProduk').val(namaProduk);
    });

    $('#btnAddItem').on('click', function (e) {
        var t = $('#dt_itemlist').DataTable();
        var jumlah = $('#txtItemQty').val();
        var harga = $('#txtHarga').val().replace("Rp.", "");
        var total = Number(jumlah) * Number(harga);
        t.row.add({
            'PenjualanItemProdukID': $('#produkID').val(),
            'PenjualanItemProduk': $('#namaProduk').val(),
            'PenjualanItemJumlah': jumlah,
            'PenjualanItemHarga': $('#txtHarga').val(),
            'PenjualanItemTotal': 'Rp.' + total
        }).draw();

        //Hitung grand total
        var grandTotal = $('#txtGrandTotal').val().replace("Rp.", "");
        var finalGT = "Rp." + (Number(grandTotal) + Number(total));
        $('#txtGrandTotal').val(finalGT);

        //reset component
        $('#ddlItemName').val($('#ddlItemName option:first-child').val()).trigger('change');
        $('#txtItemQty').val("");
        $('#txtHarga').val("");
        $('#produkID').val("");
        $('#namaProduk').val("");
    });

  function btn_Save (form) {
        var itemTable = $('#dt_itemlist').DataTable();
        var formSales = $(form);
        var items = itemTable.rows().data().toArray();

        var itemsJson = JSON.stringify(items);
        //assign items json
        $('#PenjualanItemJson').val(itemsJson);

        //assign grandtotal value
        var grandTotalValue = $('#txtGrandTotal').val().replace("Rp.", "");
        $('#PenjualanGrandTotal').val(grandTotalValue);


        var dataSource = new FormData(formSales[0]);
        $.ajax({
            url: formSales.attr("action"),
            method: 'post',
            cache: false,
            data: dataSource,
            contentType: false,
            processData: false,
            success: function (result) {
                alert("Berhasil disimpan");
            },
            error: function (err) {
                alert("Gagal menyimpan data!");
            }
        });
        return false;
    };

    $('#btnClose').on('click', function () {
        window.location = "/Sales";
    });
});