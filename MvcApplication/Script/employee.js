﻿$(document).ready(function () {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    $.ajax({
        url: '/Employee/GetEmployees',
        method: 'post',
        dataType: 'json',
        success: function (data) {
            $('#tblEmployee').dataTable({
                data: data,
                order: [[0, 'desc']],
                columns: [
                    { data: 'KaryawanID', visible: false },
                    {
                        data: function (data, type, full, meta) {
                            if (data.KaryawanFoto != null)
                                return '<img src="' + data.KaryawanFoto + '"  width = 40/>';
                            else
                                return "-";
                        }, width: '40px'
                    },
                    { data: 'KaryawanKode', width:'30px' },
                    { data: 'KaryawanNama', width:'40px' },
                    { data: 'KaryawanAlamat', width:'40px'},
                    { data: 'KaryawanTelp', width: '40px' },
                    {
                        data: function (data, type, full, meta) {
                            return '<a class="btn btn-warning" href="/Employee/Edit/' + data.KaryawanID + '">Edit</a>';
                        }, width: '25px'
                    },
                    {
                        data: function (data, type, full, meta) {
                            return '<a class="delete btn btn-danger">Delete</a>';
                        }, width: '25px'
                    }
                    
                ]
            })
        }
    })

    $('#demo-dt-addrow-btn').on('click', function () {
        window.location = "/Employee/Create";
    });

    $('#tblEmployee').on('click', 'tr a.delete', function (e) {
        e.preventDefault();
        var oTable = $('#tblEmployee').DataTable();
        var selectedRow = $(this).parents('tr')[0];
        var selectedData = oTable.row(selectedRow).data();

        //confirmation delete
        var result = confirm("Yakin data akan dihapus?");
        if (result) {
            var deleteResult;
              $.ajax({
                    url: '/Employee/Delete',
                    method: 'post',
                  async:false,
                    data: { id: selectedData.KaryawanID },
                    success: function (e) {
                        if (e.Type == 'alert-success') {
                            deleteResult = true;
                        }
                    },
                    error: function (e) {
                        alert("Gagal dihapus!");
                    }
              })
              if (deleteResult) {
                  oTable.row(selectedRow).remove().draw();
              }
        }
        
    });
})