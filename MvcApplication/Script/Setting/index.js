﻿$(document).ready(function () {
    $.fn.DataTable.ext.pager.numbers_length = 5;

    $.ajax({
        url: '/Setting/GetAllSetting',
        method: 'post',
        dataType: 'json',
        success: function (data) {
            $('#tblSetting').dataTable({
                data: data,
                order: [[0, 'desc']],
                columns: [
                    {data:'SettingID', visible:false},
                    { data: 'SettingName', width: '80px' },
                    { data: 'SettingValueFormat', width: '60px' },
                    { data: 'SettingPunishmentFormat', width: '30px' },
                    {
                        data: function (data, type, full, meta) {
                            return '<a href="" class="btn btn-warning">Edit</a>';
                        }
                    },
                    {
                        data: function (data, type, full, meta) {
                            return '<a href="" class="delete btn btn-danger">Delete</a>';
                        }
                    }
                ]
            });
        }
    });
});