﻿$(document).ready(function () {
    var frmEditUser = $('#formEditUser').validate({
        rules: {
            UserName: {
                required: true
            },
            UserPassword: {
                required: true
            },
            RetypePassword: {
                required: true,
                equalTo:'#UserPassword'
            }
        },
        messages: {
            UserName: {
                required: '-(Harus diisi)'
            },
            UserPassword: {
                required: '-(Harus diisi)'
            },
            RetypePassword: {
                required: '-(Harus diisi)',
                equalTo: 'Password & Retype Password tidak sama'
            }
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            $(element).siblings('label').append(error)
        },
        highlight: function (element) {
            $(element).siblings('label').addClass('error')
        },
        unhighlight: function (element) {
            $(element).siblings('label').removeClass('error')
        },
        submitHandler: function (form) {
            btn_Save(form);
            return false;
        }
    });
})


function btn_Save(form) {
    var $form = $(form);
    var data = new FormData($form[0]);
    $.ajax({
        url: $form.attr('action'),
        type: 'POST',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result)
                alert("Data berhasil diubah");
        },
        error: function (error) {
            alert("Data gagal diubah");
        }
    });
}

$('#btnClose').on('click', function () {
    window.location = '/User';
});

