﻿$(document).ready(function () {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    $.ajax({
        url: '/User/GetAllUsers',
        method: 'post',
        dataType: 'json',
        success: function (data) {
            var oTable = $('#tblUser').dataTable({
                data: data,
                order:[[0,'desc']],
                columns: [
                    {data:'UserID', visible: false},
                    { data: 'Username', width: '30px' },
                    { data: 'Userpassword', width: '40px' },
                    {
                        data: function (data, type, full, meta) {
                            return '<a class="btn btn-warning" href="/User/Edit/'+data.UserID+'">Edit</a>';
                        }, width: '25px'
                    },
                    {
                        data: function (data, type, full, meta) {
                            return '<a class="delete btn btn-danger">Delete</a>';
                        }, width: '25px'
                    }
                ]
            })
        }
    });

    $('#tblUser').on('click', 'tr a.delete', function (e) {
        var oTable = $('#tblUser').DataTable();
        var selectedRow = $(this).parents('tr')[0];
        var selectedData = oTable.row(selectedRow).data();
        var deleted = false;

        var confirmDelete = confirm("Yakin data akan dihapus?");
        if (confirmDelete) {
            $.ajax({
                url:'/User/Delete/',
                data: { id: selectedData.UserID },
                method: 'post',
                async: false,
                success: function (result) {
                    if (result) {
                        deleted = true;
                        alert("Berhasil menghapus data");
                    }
                },
                error:function(error){
                    alert("Gagal menghapus data");
                }
                });
        }
        if (deleted) {
            oTable.row(selectedRow).remove().draw();
        }
    });

    $('#demo-dt-addrow-btn').on('click', function () {
        window.location = "/User/Create";
    });

})