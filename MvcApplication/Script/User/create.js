﻿$(document).ready(function () {
    var $frmNewUser = $('#formNewUser').validate({
        rules: {
            UserName: {
                required: true
            },
            UserPassword: {
                required: true
            },
            RetypePassword: {
                required: true,
                equalTo: '#UserPassword'
            }
        },
        messages: {
            UserName: {
                required:'-(HARUS DIISI)'
            },
            UserPassword: {
                required:'-(HARUS DIISI)'
            },
            RetypePassword: {
                required: '-(HARUS DIISI)',
                equalTo:'-Password tidak sama'
            }
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            element.siblings("label").append(error);
        },
        highlight: function (element) {
            $(element).siblings("label").addClass("error");
        },
        unhighlight: function (element) {
            $(element).siblings("label").removeClass("error");
        },
        submitHandler: function (form) {
            btn_Save(form);
            return false;
        }
    });
});

function btn_Save(form) {
    var $form = $(form);
    var formData = new FormData($form[0]);
    $.ajax({
        url: '/User/Create/',
        type: "POST",
        cache: false,
        data: formData,
        contentType: false,
        processData: false,
        success: function (result) {
            alert("Data berhasil disimpan!");
        },
        error: function (e) {
            alert(e);
        }
    });
}

$('#btnClose').on('click', function () {
    window.location = "/User";
});