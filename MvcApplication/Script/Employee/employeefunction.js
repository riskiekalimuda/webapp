﻿$(document).ready(function () {
    //Validation
    var $newFormKaryawan = $('#formNewKaryawan').validate({
        rules: {
            KaryawanKode: {
                required: true
            },
            KaryawanNama: {
                required: true
            },
            KaryawanAlamat: {
                required: true
            },
            KaryawanTelp: {
                required: true
            }
        },
        messages: {
            KaryawanKode: {
                required: '-(HARUS DIISI)'
            },
            KaryawanNama: {
                required: '-(HARUS DIISI)'
            },
            KaryawanAlamat: {
                required: '-(HARUS DIISI)'
            },
            KaryawanTelp: {
                required: '-(HARUS DIISI)'
            }
        },
        errorElement: "span",
        errorPlacement: function (error, element) {
            $(element).siblings("label").append(error);
        },
        highlight: function (element) {
            $(element).siblings("label").addClass("error");
        },
        unhighlight: function (element) {
            $(element).siblings("label").removeClass("error");
        },
        submitHandler: function (form) {
            btn_Save(form);
            return false;
        }
    });
    //End validation
});

$('#btnClose').on('click', function () {
    window.location = "/Employee";
});

function btn_Save(form) {
    var $form = $(form);
        var formData = new FormData($form[0]);
        $.ajax({
            url: '/Employee/Create/',
            type: "POST",
            cache: false,
            data: formData,
            contentType: false,
            processData: false,
            success: function (msg) {
                if (msg != null) {
                    alert("success!");
                }
            },
            error: function (err) {
                alert("gagal!");
                console.log(err);
            }
        });
        return false;
};


$('#btnEdit').on('click', function () {
    var $form = $('#formEditKaryawan');
    var formData = new FormData($form[0]);
    $.ajax({
        url: '/Employee/Edit/',
        type: "POST",
        cache: false,
        data: formData,
        contentType: false,
        processData: false,
        success: function (msg) {
            if (msg != null) {
                alert("success!");
            }
        },
        error: function (err) {
            alert("gagal!");
            console.log(err);
        }
    });
    return false;
});

