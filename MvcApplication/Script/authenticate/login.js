﻿$(document).ready(function () {
    var loginForm = $('#loginform').validate({
        rules: {
            Username: {
                required: true
            },
            Password: {
                required:true
            }
        },
        messages: {
            Username: {
                required: '-(HARUS DIISI)'
            },
            Password: {
                required: '-(HARUS DIISI)'
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            $(element).siblings('label').append(error);
        },
        highlight: function (error, element) {
            $(element).siblings('label').addClass('error');
        },
        unhighlight: function (error, element) {
            $(element).siblings('label').removeClass('error');
        }
    });
});