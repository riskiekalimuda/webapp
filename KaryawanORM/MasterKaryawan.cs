﻿using Common;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using MMI.Extensions;

namespace KaryawanORM
{
    public class MasterKaryawan
    {
        public static Boolean CekFormatExtensiFoto(String extSelected)
        {
            Boolean result = false;
            String[] ext = new String[] { ".jpg", ".jpeg", ".png" };
            if (!String.IsNullOrEmpty(extSelected))
            {
                String selected = extSelected.ToLower();
                foreach (var item in ext)
                {
                    if (item.Equals(selected))
                    {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }
        public static List<ExportPdfLookUp> GenerateExportPdf()
        {
            List<ExportPdfLookUp> result = new List<ExportPdfLookUp>();
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queris = from k in context.MstKaryawans
                                 where k.KaryawanIsDelete == 0
                                 select k;
                    if (queris != null && queris.Count() > 0)
                    {
                        foreach (var item in queris)
                        {
                            ExportPdfLookUp data = new ExportPdfLookUp();
                            data.KaryawanID = item.KaryawanID;
                            data.KaryawanKode = item.KaryawanKode;
                            data.KaryawanNama = item.KaryawanNama;
                            data.KaryawanAlamat = item.KaryawanAlamat;
                            data.KaryawanTelp = item.KaryawanTelp;
                            result.Add(data);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static DataTablesRecordWrapper<MasterKaryawanGrid> GridSearchEntity(DataTablesPostParam postParam)
        {
            DataTablesRecordWrapper<MasterKaryawanGrid> result = new DataTablesRecordWrapper<MasterKaryawanGrid>();
            try
            {
                result.sEcho = postParam.Echo;
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {

                    var queries = from k in context.MstKaryawans
                                  where k.KaryawanIsDelete == 0
                                  select new MasterKaryawanGrid
                                  {
                                      KaryawanID = k.KaryawanID,
                                      KaryawanKode = k.KaryawanKode,
                                      KaryawanNama = k.KaryawanNama,
                                      KaryawanAlamat = k.KaryawanAlamat,
                                      KaryawanTelp = k.KaryawanTelp,
                                      KaryawanFoto = k.KaryawanFoto
                                  };

                    if (queries != null && queries.Count() > 0)
                    {
                        result.iTotalRecords = queries.Count();

                        // Do Filter
                        foreach (var SearchParam in postParam.Searches)
                        {

                            if (SearchParam.Column == 0)
                                queries = queries.Where(x => x.KaryawanKode.Contains(SearchParam.Search));
                            else if (SearchParam.Column == 2)
                                queries = queries.Where(x => x.KaryawanNama.Contains(SearchParam.Search));
                            else if (SearchParam.Column == 3)
                                queries = queries.Where(x => x.KaryawanAlamat.Contains(SearchParam.Search));
                            else if (SearchParam.Column == 4)
                                queries = queries.Where(x => x.KaryawanTelp.Contains(SearchParam.Search));
                        }

                        foreach (var SortParam in postParam.Sorts)
                        {
                            if (SortParam.SortDirection != null)
                            {
                                // kolom region
                                if (SortParam.Column == 0 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.KaryawanID);
                                else if (SortParam.Column == 0 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.KaryawanID);
                                // kolom group
                                else if (SortParam.Column == 1 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.KaryawanKode);
                                else if (SortParam.Column == 1 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.KaryawanKode);
                                // kolom code
                                else if (SortParam.Column == 2 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.KaryawanNama);
                                else if (SortParam.Column == 2 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.KaryawanNama);
                                // kolom company name
                                else if (SortParam.Column == 3 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.KaryawanAlamat);
                                else if (SortParam.Column == 3 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.KaryawanAlamat);

                            }
                        }
                        result.iTotalDisplayRecords = queries.Count();
                        if (postParam.DisplayLength <= 0)
                            postParam.DisplayLength = 200;

                        result.aaData = queries.ToList().Page(postParam.DisplayLength, postParam.DisplayStart).ToList();
                    }
                    else
                    {
                        result.aaData = new List<MasterKaryawanGrid>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static Boolean CekKaryawanByKode(String kode)
        {
            Boolean result = false;
            if (!String.IsNullOrEmpty(kode))
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var queries = from k in context.MstKaryawans
                                      where k.KaryawanIsDelete == 0 &&
                                      k.KaryawanKode.Equals(kode)
                                      select k;
                        if (queries != null && queries.Count() > 0)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static Int32 Insert(MstKaryawan karyawan)
        {
            Int32 result = 0;
            if (karyawan != null)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        context.MstKaryawans.Add(karyawan);
                        result = context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return result;
        }

        public static Int32 Update(MstKaryawan karyawan)
        {
            Int32 result = 0;
            if (karyawan != null)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        context.Entry(karyawan).State = System.Data.Entity.EntityState.Modified;
                        result = context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static Int32 Delete(Int64 id)
        {
            Int32 result = 0;
            if (id > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var queries = from k in context.MstKaryawans
                                      where k.KaryawanIsDelete == 0 && k.KaryawanID == id
                                      select k;
                        if (queries != null && queries.Count() > 0)
                        {
                            var obj = queries.First();
                            obj.KaryawanIsDelete = 1;
                            context.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            result = context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static MstKaryawan GetKaryawanByID(Int64 id)
        {
            MstKaryawan result = null;
            if (id > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var queries = from k in context.MstKaryawans
                                      where k.KaryawanIsDelete == 0 && k.KaryawanID == id
                                      select k;
                        if (queries != null && queries.Count() > 0)
                        {
                            var obj = queries.First();
                            result = obj;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }
        public static List<MasterKaryawanGrid> GetAllKaryawansForGrid()
        {
            List<MasterKaryawanGrid> result = null;
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queries = context.MstKaryawans.Where(x => x.KaryawanIsDelete == 0).Select(x => new MasterKaryawanGrid
                    {
                        KaryawanID = x.KaryawanID,
                        KaryawanKode = x.KaryawanKode,
                        KaryawanFoto = x.KaryawanFoto,
                        KaryawanNama = x.KaryawanNama,
                        KaryawanAlamat = x.KaryawanAlamat,
                        KaryawanTelp = x.KaryawanTelp
                    });
                    if (queries != null && queries.Count() > 0)
                    {
                        result = queries.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static List<MstKaryawan> GetAllKaryawans()
        {
            List<MstKaryawan> result = null;
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queries = from k in context.MstKaryawans
                                  where k.KaryawanIsDelete == 0
                                  select k;
                    if (queries != null && queries.Count() > 0)
                    {
                        result = queries.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static Boolean IsDataExist(Int64 Id, String Kode)
        {
            Boolean result = false;
            if (Id > 0 && !String.IsNullOrEmpty(Kode))
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var queries = from k in context.MstKaryawans
                                      where Id != k.KaryawanID && Kode == k.KaryawanKode
                                      && k.KaryawanIsDelete == 0
                                      select k;
                        if (queries != null && queries.Count() > 0)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }
    }
}
