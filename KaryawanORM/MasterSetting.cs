﻿using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM
{
    public class MasterSetting
    {
        public static List<MasterSettingGrid> GetAllSettingForGrid()
        {
            List<MasterSettingGrid> result = null;
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queries = from s in context.MstSettings
                                  where !s.SettingIsDelete
                                  select new MasterSettingGrid
                                  {
                                      SettingID = s.SettingID,
                                       SettingName = s.SettingName,
                                        SettingPunishment = s.SettingPunishment,
                                         SettingValue = s.SettingValue
                                  };
                    if (queries != null && queries.Count() > 0)
                    {
                        result = queries.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Gagal query setting " + ex.Message);
            }
            return result;
        }
    }
}
