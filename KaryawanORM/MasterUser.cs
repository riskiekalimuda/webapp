﻿using EncryptDecryptUtilities;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM
{
    public class MasterUser
    {
        public static MasterUserLookUp GetUserByUserLookUp(Int64 id)
        {
            MasterUserLookUp result = null;
            if (id > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var query = context.MstUsers.Where(x => x.UserID == id && x.UserIsDelete == false).First();
                        if (query != null)
                        {
                            result = new MasterUserLookUp();
                            result.UserID = query.UserID;
                            result.UserName = query.Username;
                            result.UserPassword = EncryptDecryptString.DecryptString(query.UserPassword);
                            return result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Ambil data user gagal " + ex.Message);
                }
            }
            return result;
        }

        public static Int32 UpdateUserByLookUp(MasterUserLookUp userLookUp)
        {
            Int32 result = 0;
            if (userLookUp != null)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var query = context.MstUsers.Where(x => x.UserID == userLookUp.UserID && x.UserIsDelete == false).First();
                        if (query != null)
                        {
                            query.Username = userLookUp.UserName;
                            query.UserPassword = userLookUp.UserPassword;
                            context.Entry(query).State = System.Data.Entity.EntityState.Modified;
                            result = context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Update user gagal " + ex.Message); 
                }
            }
            return result;
        }
        
        public static Int32 InsertUserByLookUp(MasterUserLookUp userLookUp)
        {
            Int32 result = 0;
            if (userLookUp != null)
            {
                try
                {
                    String encrypted = EncryptDecryptUtilities.EncryptDecryptString.EncryptString(userLookUp.UserPassword);
                    MstUser newData = new MstUser()
                    {
                         Username = userLookUp.UserName,
                         UserPassword = encrypted
                    };
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        context.MstUsers.Add(newData);
                        result = context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Insert User failed!" + ex.Message);
                }
            }
            return result;
        }
        //public static Int32 InserUser
        public static Boolean IsUserHaveLogin(String username, String password)
        {
            Boolean result = false;
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        String encriptPwd = EncryptDecryptString.EncryptString(password);
                        var query = context.MstUsers.Where(x => x.Username.Equals(username) && x.UserPassword.Equals(encriptPwd) && x.UserIsDelete == false).FirstOrDefault();
                        if (query != null)
                            result = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static List<MasterUsersGrid> GetAllUsers()
        {
            List<MasterUsersGrid> result = null;
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queries = context.MstUsers.Where(x => x.UserIsDelete == false).Select(x => new MasterUsersGrid
                    {
                        UserID = x.UserID,
                        Username = x.Username,
                        Userpassword = x.UserPassword
                    });
                    if (queries != null && queries.Count() > 0)
                    {
                        result = queries.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static Int32 Delete(Int64 id)
        {
            Int32 result = 0;
            if (id > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var query = context.MstUsers.Where(x => x.UserID == id && x.UserIsDelete == false).First();
                        if (query != null)
                        {
                            query.UserIsDelete = true;
                            context.Entry(query).State = System.Data.Entity.EntityState.Modified;
                            result = context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Gagal menghapus data " + ex.Message);
                }
            }
            return result;
        }
    }
}
