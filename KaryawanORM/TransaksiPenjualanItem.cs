﻿using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM
{
    public class TransaksiPenjualanItem
    {
        public static Int32 Insert(TrxPenjualanItem penjualanItem)
        {
            Int32 result = 0;
            if (penjualanItem != null)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        context.TrxPenjualanItems.Add(penjualanItem);
                        result = context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static List<PenjualanItemDetailGrid> GetAllPenjualanItemByPenjualanID(Int64 penjualanID)
        {
            List<PenjualanItemDetailGrid> result = null;
            if (penjualanID > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var queries = from pItem in context.TrxPenjualanItems
                                      join prd in context.MstProduks
                                      on pItem.PenjualanItemProdukID equals prd.ProdukID
                                      where pItem.PenjualanItemPenjualanID == penjualanID
                                      && !pItem.PenjualanItemIsDelete
                                      select new 
                                      {
                                        PenjualanItemProdukID = prd.ProdukID,
                                         PenjualanItemProduk = prd.ProdukNama,
                                          PenjualanItemJumlah = pItem.PenjualanItemJumlah,
                                           PenjualanItemHarga = pItem.PenjualanItemHarga,
                                            PenjualanItemTotal = pItem.PenjualanItemTotal
                                      };
                        if (queries != null && queries.Count() > 0)
                        {
                            result = new List<PenjualanItemDetailGrid>();
                            foreach (var item in queries)
                            {
                                PenjualanItemDetailGrid dataGrid = new PenjualanItemDetailGrid();
                                dataGrid.PenjualanItemProdukID = item.PenjualanItemProdukID;
                                dataGrid.PenjualanItemProduk = item.PenjualanItemProduk;
                                dataGrid.PenjualanItemJumlah = item.PenjualanItemJumlah;
                                dataGrid.PenjualanItemHarga = "Rp." + item.PenjualanItemHarga.ToString();
                                dataGrid.PenjualanItemTotal = "Rp." + item.PenjualanItemTotal.ToString();
                                result.Add(dataGrid);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static Int32 Delete(Int64 penjualanItemID)
        {
            Int32 result = 0;
            if (penjualanItemID > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var query = from pItem in context.TrxPenjualanItems
                                    where !pItem.PenjualanItemIsDelete && pItem.PenjualanItemID == penjualanItemID
                                    select pItem;
                        if (query != null && query.Count() > 0)
                        {
                            var obj = query.First();
                            obj.PenjualanItemIsDelete = true;
                            context.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            result = context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static Int32 DeletePenjualanItemByPenjualanID(Int64 penjualanID)
        {
            Int32 result = 0;
            if (penjualanID > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var queries = from pItem in context.TrxPenjualanItems
                                      where pItem.PenjualanItemPenjualanID == penjualanID &&
                                      !pItem.PenjualanItemIsDelete
                                      select pItem;
                        if (queries != null && queries.Count() > 0)
                        {
                            foreach (var item in queries)
                            {
                                result += Delete(item.PenjualanItemID);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }
    }
}
