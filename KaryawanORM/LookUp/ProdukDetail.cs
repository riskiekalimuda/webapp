﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class ProdukDetail
    {
        public Int64 ProdukID { get; set; }
        public decimal? ProdukHarga { get; set; }
        public String ProdukNama { get; set; }
        public String ProdukNamaFormat
        {
            get
            {
                String format = "";
                if(ProdukHarga != null)
                {
                    format = ProdukNama+"\n"+ "Rp." +  ProdukHarga.Value.ToString("#.###");
                }
                return format;
            }
        }
    }
}
