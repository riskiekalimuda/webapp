﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class MasterSettingGrid
    {
        public Int64 SettingID { get; set; }
        public String SettingName { get; set; }
        public TimeSpan SettingValue { get; set; }
        public Decimal? SettingPunishment { get; set; }
        public String SettingValueFormat
        {
            get
            {
                String formatResult = SettingValue.ToString(@"hh\:mm\:ss");
                return formatResult;
            }
        }
        public String SettingPunishmentFormat
        {
            get
            {
                String formatResult = "";
                if (SettingPunishment != null)
                    formatResult = SettingPunishment.Value.ToString("Rp." + "#.###");
                return formatResult;
            }
        }
    }
}
