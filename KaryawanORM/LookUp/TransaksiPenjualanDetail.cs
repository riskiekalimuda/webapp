﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class TransaksiPenjualanDetail
    {
        
        public Int64 PenjualanID { get; set; }

        [Display(Name="Karyawan")]
        public Int64 PenjualanKaryawanID { get; set; }

        [Display(Name = "Tanggal")]
        public DateTime PenjualanTanggal { get; set; }
        public decimal PenjualanGrandTotal { get; set; }
        public String PenjualanItemJson { get; set; }
        public IEnumerable<PenjualanItemDetailLookUp> PenjualanItemDetail { get; set; }
    }
}
