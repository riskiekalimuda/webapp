﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class TransaksiPenjualanGrid
    {
        public Int64 PenjualanID { get; set; }
        public DateTime PenjualanTanggal { get; set; }
        public String PenjualanTanggalFormat
        {
            get
            {
                String frmt = "";
                frmt = PenjualanTanggal.ToString("dd MMM yyyy");
                return frmt;
            }
        }
        public String PenjualanKaryawanNama { get; set; }
        public Decimal PenjualanGrandTotal { get; set; }
        public String PenjualanGrandTotalFormat
        {
            get
            {
                String frmt = "";
                frmt = "Rp. " + PenjualanGrandTotal.ToString();
                return frmt;
            }
        }

    }
}
