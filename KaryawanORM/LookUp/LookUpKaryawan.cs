﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
using System.ComponentModel;

namespace KaryawanORM.LookUp
{
    public class LookUpKaryawan
    {
        public Int64 KaryawanID { get; set; }
        
        [DisplayName("Kode Karyawan")]
        public String KaryawanKode { get; set; }
        
        [DisplayName("Nama Karyawan")]
        public String KaryawanNama { get; set; }
        
        [DisplayName("Alamat")]
        public String KaryawanAlamat { get; set; }
        
        [DisplayName("No. Telp")]
        public String KaryawanTelp { get; set; }

        [DisplayName("Foto Karyawan")]
        public HttpPostedFileBase KaryawanFoto { get; set; }

        public String KaryawanFotoFileName { get; set; }
    }
}
