﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;



namespace KaryawanORM.LookUp
{
    public class PenjualanItemDetailLookUp
    {
        public Int64 PenjualanItemID { get; set; }
        public Int64 PenjualanItemProdukID { get; set; }
        public String PenjualanItemProduk { get; set; }
        public decimal PenjualanItemJumlah { get; set; }
        public decimal PenjualanItemHarga { get; set; }
        public decimal PenjualanItemTotal { get; set; }


        public static List<PenjualanItemDetailLookUp> ConvertPenjualanItemFromJson(String jsonItems)
        {
            List<PenjualanItemDetailLookUp> result = new List<PenjualanItemDetailLookUp>();
            if (!String.IsNullOrWhiteSpace(jsonItems))
            {
                try
                {
                    var orderItem = JsonConvert.DeserializeObject<PenjualanItemDetailGrid[]>(jsonItems);
                    if (orderItem != null)
                    {
                        foreach (var item in orderItem)
                        {
                            decimal harga = Convert.ToDecimal(item.PenjualanItemHarga.Replace("Rp.", ""));
                            decimal total = Convert.ToDecimal(item.PenjualanItemTotal.Replace("Rp.", ""));
                            PenjualanItemDetailLookUp penjualanItem = new PenjualanItemDetailLookUp()
                            {
                                PenjualanItemID = item.PenjualanItemDetailID,
                                PenjualanItemProdukID = item.PenjualanItemProdukID,
                                PenjualanItemProduk = item.PenjualanItemProduk,
                                PenjualanItemJumlah = item.PenjualanItemJumlah,
                                PenjualanItemHarga = harga,
                                PenjualanItemTotal = total
                            };
                            result.Add(penjualanItem);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }
    }
}
