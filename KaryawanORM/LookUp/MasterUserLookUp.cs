﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class MasterUserLookUp
    {
        public Int64 UserID { get; set; }

        [DisplayName("User Name")]
        public String UserName { get; set; }

        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public String UserPassword { get; set; }
    }
}
