﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class ParamLogin
    {
        [Required]
        [Display(Name="Username")]
        public String Username { get; set; }

        [Required]
        [Display(Name="Password")]
        public String Password { get; set; }

        [Display(Name="Remember Me")]
        public bool RememberMe { get; set; }
    }
}
