﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class MasterKaryawanGrid
    {
        public Int64 KaryawanID { get; set; }
        public String KaryawanKode { get; set; }
        public String KaryawanNama { get; set; }
        public String KaryawanAlamat { get; set; }
        public String KaryawanTelp { get; set; }
        public String KaryawanFoto { get; set; }
    }
}
