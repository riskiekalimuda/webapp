﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM.LookUp
{
    public class PenjualanItemDetailGrid
    {
        public Int64 PenjualanItemDetailID { get; set; }
        public Int64 PenjualanItemProdukID { get; set; }
        public String PenjualanItemProduk { get; set; }
        public decimal PenjualanItemJumlah { get; set; }
        public String PenjualanItemHarga { get; set; }
        public String PenjualanItemTotal { get; set; }
    }
}
