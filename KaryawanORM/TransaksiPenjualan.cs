﻿using Common;
using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMI.Extensions;
using Newtonsoft.Json;

namespace KaryawanORM
{
    public class TransaksiPenjualan
    {
        public static TransaksiPenjualanDetail GetPenjualanDetailByPenjualanID(Int64 penjualanID)
        {
            TransaksiPenjualanDetail result = null;
            if (penjualanID != null && penjualanID > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var queries = from k in context.MstKaryawans
                                      join p in context.TrxPenjualans
                                      on k.KaryawanID equals p.PenjualanKaryawanID
                                      where !p.PenjualanIsDelete && p.PenjualanID == penjualanID
                                      select new TransaksiPenjualanDetail
                                      {
                                          PenjualanID = p.PenjualanID,
                                          PenjualanKaryawanID = k.KaryawanID,
                                          PenjualanTanggal = p.PenjualanTanggal,
                                          PenjualanGrandTotal = p.PenjualanGrandTotal
                                      };
                        if (queries != null && queries.Count() > 0)
                        {
                            TransaksiPenjualanDetail obj = queries.First();
                            obj.PenjualanItemJson = JsonConvert.SerializeObject(TransaksiPenjualanItem.GetAllPenjualanItemByPenjualanID(penjualanID));
                            result = obj;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static Int32 Insert(TrxPenjualan penjualan)
        {
            Int32 result = 0;
            if (penjualan != null)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        context.TrxPenjualans.Add(penjualan);
                        result = context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static Int32 DeletePenjualanDetail(Int64 penjualanID)
        {
            Int32 result = 0;
            if (penjualanID > 0)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        var query = from p in context.TrxPenjualans
                                    where !p.PenjualanIsDelete && p.PenjualanID == penjualanID
                                    select p;
                        if (query != null && query.Count() > 0)
                        {
                            var obj = query.First();
                            obj.PenjualanIsDelete = true;
                            context.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                            result = context.SaveChanges();
                            result += TransaksiPenjualanItem.DeletePenjualanItemByPenjualanID(penjualanID);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static Int32 Update(TrxPenjualan penjualan)
        {
            Int32 result = 0;
            if (penjualan != null)
            {
                try
                {
                    using (KaryawanDbEntities context = new KaryawanDbEntities())
                    {
                        context.Entry(penjualan).State = System.Data.Entity.EntityState.Modified;
                        result = context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return result;
        }

        public static List<TransaksiPenjualanGrid> GetAllSalesForGridSource()
        {
            List<TransaksiPenjualanGrid> lstData = null;
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queries = from p in context.TrxPenjualans
                                  join k in context.MstKaryawans
                                  on p.PenjualanKaryawanID equals k.KaryawanID
                                  where !p.PenjualanIsDelete
                                  select new TransaksiPenjualanGrid
                                  {
                                      PenjualanTanggal = p.PenjualanTanggal,
                                      PenjualanKaryawanNama = k.KaryawanNama,
                                      PenjualanGrandTotal = p.PenjualanGrandTotal,
                                      PenjualanID = p.PenjualanID
                                  };
                    if (queries != null && queries.Count() > 0)
                    {
                        lstData = queries.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstData;
        }

        public static DataTablesRecordWrapper<TransaksiPenjualanGrid> GridSearchPenjualan(DataTablesPostParam postParam)
        {
            DataTablesRecordWrapper<TransaksiPenjualanGrid> result = new DataTablesRecordWrapper<TransaksiPenjualanGrid>();
            try
            {
                result.sEcho = postParam.Echo;
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {

                    var queries = from k in context.MstKaryawans
                                  join p in context.TrxPenjualans
                                  on k.KaryawanID equals p.PenjualanKaryawanID
                                  where !p.PenjualanIsDelete
                                  select new TransaksiPenjualanGrid
                                  {
                                      PenjualanID = p.PenjualanID,
                                      PenjualanTanggal = p.PenjualanTanggal,
                                      PenjualanKaryawanNama = k.KaryawanNama,
                                      PenjualanGrandTotal = p.PenjualanGrandTotal
                                  };

                    if (queries != null && queries.Count() > 0)
                    {
                        result.iTotalRecords = queries.Count();

                        // Do Filter
                        foreach (var SearchParam in postParam.Searches)
                        {

                            if (SearchParam.Column == 0)
                                queries = queries.Where(x => x.PenjualanTanggal.ToString().Contains(SearchParam.Search));
                            else if (SearchParam.Column == 2)
                                queries = queries.Where(x => x.PenjualanKaryawanNama.Contains(SearchParam.Search));
                            else if (SearchParam.Column == 3)
                                queries = queries.Where(x => x.PenjualanGrandTotal.ToString().Contains(SearchParam.Search));
                        }

                        foreach (var SortParam in postParam.Sorts)
                        {
                            if (SortParam.SortDirection != null)
                            {
                                // kolom region
                                if (SortParam.Column == 0 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.PenjualanID);
                                else if (SortParam.Column == 0 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.PenjualanID);
                                // kolom group
                                else if (SortParam.Column == 1 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.PenjualanTanggal);
                                else if (SortParam.Column == 1 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.PenjualanTanggal);
                                // kolom code
                                else if (SortParam.Column == 2 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.PenjualanKaryawanNama);
                                else if (SortParam.Column == 2 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.PenjualanKaryawanNama);
                                // kolom company name
                                else if (SortParam.Column == 3 && SortParam.SortDirection.ToLower().Equals("desc"))
                                    queries = queries.OrderByDescending(x => x.PenjualanGrandTotal);
                                else if (SortParam.Column == 3 && SortParam.SortDirection.ToLower().Equals("asc"))
                                    queries = queries.OrderBy(x => x.PenjualanGrandTotal);

                            }
                        }
                        result.iTotalDisplayRecords = queries.Count();
                        if (postParam.DisplayLength <= 0)
                            postParam.DisplayLength = 200;

                        result.aaData = queries.ToList().Page(postParam.DisplayLength, postParam.DisplayStart).ToList();
                    }
                    else
                    {
                        result.aaData = new List<TransaksiPenjualanGrid>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
