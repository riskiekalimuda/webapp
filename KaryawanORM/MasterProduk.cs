﻿using KaryawanORM.LookUp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaryawanORM
{
    public class MasterProduk
    {
        public static List<MstProduk> GetAllProduks()
        {
            List<MstProduk> lstProduk = null;
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queries = context.MstProduks.Where(x => x.ProdukIsDelete == false).ToList();
                    if (queries != null && queries.Count() > 0)
                        lstProduk = queries;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstProduk;
        }

        public static List<ProdukDetail> GetAllProdukDetails()
        {
            List<ProdukDetail> lstProduk = null;
            try
            {
                using (KaryawanDbEntities context = new KaryawanDbEntities())
                {
                    var queries = from p in context.MstProduks
                                  where !p.ProdukIsDelete
                                  select new ProdukDetail
                                  {
                                      ProdukID = p.ProdukID,
                                      ProdukNama = p.ProdukNama,
                                      ProdukHarga = p.ProdukHarga
                                  };
                    if (queries != null && queries.Count() > 0)
                    {
                        lstProduk = queries.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstProduk;
        }
    }
}
