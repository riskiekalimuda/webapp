//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KaryawanORM
{
    using System;
    using System.Collections.Generic;
    
    public partial class MstKaryawan
    {
        public MstKaryawan()
        {
            this.TrxPenjualans = new HashSet<TrxPenjualan>();
            this.TrxAbsens = new HashSet<TrxAbsen>();
        }
    
        public long KaryawanID { get; set; }
        public string KaryawanKode { get; set; }
        public string KaryawanNama { get; set; }
        public string KaryawanAlamat { get; set; }
        public string KaryawanTelp { get; set; }
        public string KaryawanFoto { get; set; }
        public short KaryawanIsDelete { get; set; }
    
        public virtual ICollection<TrxPenjualan> TrxPenjualans { get; set; }
        public virtual ICollection<TrxAbsen> TrxAbsens { get; set; }
    }
}
